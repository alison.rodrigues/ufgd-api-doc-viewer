import * as types from "./mutation-types"

export default {
	[types.SET_JSON](state, payload){
		state.JSON = payload;
	},
	[types.SET_TESTE](state, payload){
		state.TESTE = payload;
	}
}