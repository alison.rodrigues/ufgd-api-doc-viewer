import * as types from "./mutation-types"

export default {
	[types.SET_API_URL_BASE](state, payload){
		state.API_URL_BASE = payload
	},
	[types.SET_JSON_API_DISCOVERY](state, payload){
		state.JSON_API_DISCOVERY = payload
	},
	[types.SET_SNACK_MESSAGE](state, message){
		state.snackbar.message = message
	},
	[types.SET_SNACK_VIEW](state, payload){
		state.snackbar.show = payload
	},
	[types.SET_THEME](state, payload){
		state.THEME = payload
	}
}