import * as types from "./mutation-types"
import { $axios } from "../../plugins/axios"

export const getApiData = ({ commit, dispatch }, URL_BASE, ) => {
	$axios.get(URL_BASE).then(response => {
		commit(types.SET_JSON_API_DISCOVERY, response.data)
		commit(types.SET_API_URL_BASE, URL_BASE)
	}).catch(e => {
		dispatch('showSnack', 'DESCULPE <br/>Não foi possivel localizar a API, verifique o log', { root: true })
		commit(types.SET_JSON_API_DISCOVERY, "")
		console.log(e)
	});
	
}

export const showSnack = ({commit}, message) => {
	commit(types.SET_SNACK_MESSAGE, message)
	return commit(types.SET_SNACK_VIEW, true)
}

export const hideSnack = ({commit}) => {
	return commit(types.SET_SNACK_VIEW, false)
}