import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import treeViewJson from './plugins/treeViewJson';
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,
  treeViewJson,
  render: h => h(App)
}).$mount('#app')
