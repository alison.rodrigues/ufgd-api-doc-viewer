# UFGD API Doc Viewer

## Project setup
- `npm install` install dependencies
- `npm run serve` Compiles and hot-reloads for development 
- `npm run build` Compiles and minifies for production
- `npm run lint` Lints and fixes files
<br/>

### Customize configuration
 See [Configuration Reference](https://cli.vuejs.org/config/).

<hr/>

## Motivação
Ao estar desenvolvendo um projeto médio-grande, sentimos a necessidade de visualizar as opções de end-points que temos nas API's e saber qual é a estrutura dos objetos que temos que enviar. Se houvesse uma forma de visualizar essas informações seria de grande ajuda. 

Logo vem em pensamento a ideia de documentar as API's, porém escolher uma ferramenta de documentação e documentar todos os métodos já escritos, as vezes tem um custo que não a equipe não deseja assumir.

Mas todas as API's construídas para se conectarem com o gateway, já enviam seus end-points com nome e uma pequena descrição. E nesse momento é justamente a documentação dos recursos que temos a maior necessidade.

## Objetivo
Esse projeto tem por objetivo ler o json enviado pela API ao gateway através da rota `/discovery` que é onde fica o recurso que envia todos os recursos da API para o gateway. Além de conseguir ler esse json, montar uma apresentação de fácil entendimento para quem está querendo visualizar os recursos da API de forma humanizada.


Esse projeto no momento é stand-alone ele não depende de nenhum outro projeto ou recurso para funcionar, não precisa de banco de dados ou outra ferramenta. Mas a idéia é que ele seja incorporado ao projeto do gateway para algo como um portal de documentação...
## O projeto
Esse visualizador de API's é stand-alone e foi baseado no projeto do Swagger-UI, que consiste em apenas um front-end que sabe interpretar uma determinada estrutura, nesse caso o Json Discovery dos micro-serviços da UFGD.

## print-screens
![img-1 screenshot](screenshots/Screenshot-5.png)
img-1 <br/>
![img-2 screenshot](screenshots/Screenshot-1.png)
img-2 <br/>
![img-3 screenshot](screenshots/Screenshot-2.png)
img-3 <br/>
![img-4 screenshot](screenshots/Screenshot-3.png)
img-4 <br/>
![img-5 screenshot](screenshots/Screenshot-4.png)
img-5 <br/>
![img-6 screenshot](screenshots/Screenshot-6.png)
img-6 <br/>